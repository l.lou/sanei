import mimetypes, posixpath, sqlite3
from . import db as datab, conf

mimetypes.init() # try to read system mime.types
extensions_map = mimetypes.types_map.copy()
extensions_map.update({
    '': 'application/octet-stream', # Default
    '.py': 'text/plain',
    '.c': 'text/plain',
    '.h': 'text/plain',
    })

def guess_type(path):
    """Guess the type of a file.
    Argument is a PATH (a filename).
    Return value is a string of the form type/subtype,
    usable for a MIME Content-type header.
    The default implementation looks the file's extension
    up in the table self.extensions_map, using application/octet-stream
    as a default; however it would be permissible (if
    slow) to look inside the data to make a better guess.
    """
    base, ext = posixpath.splitext(path)
    if ext in extensions_map:
        return extensions_map[ext]
    ext = ext.lower()
    if ext in extensions_map:
        return extensions_map[ext]
    else:
        return extensions_map['']

def dbact(query, params):
    db = datab.get_db()
    c = db.cursor()
    print('[dbact] query: ', query)
    print('[dbact] params: ', params)
    c.execute(query, (params, ))
    res_list = c.fetchall()
    print('[dbact] res_list: ', res_list)
    if len(res_list) > 1:
        return res_list
    elif len(res_list) == 1:
        return res_list[0]
    return None

