const base_title = '[admin] Sanei'
const base_url = document.location.protocol + '//' + document.location.host + '/admin'
const default_page = 'admin'

function formhandler(e) {
	console.log('[formhandler] catched : ', e.target)
	e.preventDefault()
	let inputs = e.target.getElementsByTagName('*')
	let datas = {}
	for (i of inputs) {
		if ((i.name || 'name' in i.attributes) && (!i.value || !/^{.*}+$/.test(i.value))) {
			let n = (i.name ? i.name : i.attributes['name'].value)
			datas[n] = (i.value ? i.value : i.innerHTML)
		}
	}
	datas['cat'] = document.getElementById('cat-select').value
	if (typeof(datas['date']) !== 'undefined')
		datas['date'] = Date.now()
	//	console.log('[formhandler] form datas: ', datas)
	//	console.log('[formhandler] target url: ', e.target.action)
	requ(e.target.action, {method: 'post'}, datas)
		.then(s => console.log('[formhandler] posted form: ', s),
			e => console.log('[formhandler] error posting form: ', e))
}

/*
	requ(e.target.action, {method: 'post'}, datas)
		.then(s => console.log('[formhandler] posted form: ', s),
			e => console.log('[formhandler] error posting form: ', e))
*/

function post_article(article) {
	log('VERB', '[post_article] posting: ', article)
	requ('/admin/edit/article', {method: 'post'}, article)
		.then(s => log(LOGLVL.INFO, '[post_article] posted form: ', s),
			e => log(LOGLVL.INFO, '[post_article] error posting form: ', e))
}

function post_image(e) {
	e.preventDefault()
	log('VERB', 'id:', document.getElementById('article_id').value)
	let f = document.createElement('input'),
		reader = new FileReader(),
		id = document.getElementById('article_id').value
	reader.addEventListener('load', (e) => {
		log('VERB', '[post_image] upload file for article ', id)
		//post_article({id: id, photo: e.target.result})
		requ('/admin/addphoto/' + id, {method: 'post'}, {base64: e.target.result})
			.then(s => {
				log(LOGLVL.INFO, '[post_image] posted form: ', s)
				notif(s)
			},
				e => log(LOGLVL.INFO, '[post_image] error posting form: ', e))
	})
	f.type='file'
	f.addEventListener('change', e => {
		log('VERB', '[post_image] file choosed')
		let newImage = e.srcElement.files[0]
		reader.readAsDataURL(newImage)
	})
	log('VERB', '[post_image] choose file')
	f.click()
}

/*
function post_image(e) {
	e.preventDefault()
	var f = document.createElement('input'),
		reader = new FileReader(),
		xhr = new XMLHttpRequest()
	xhr.onreadystatechange = function(){
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				log('INFO', '[post_image] success. resp: ', xhr.response)
			} else
				log('WARN', '[post_image] HTTP error ', xhr)
		} else
			log('VERB', '[post_image] XHR state change ', xhr.statusText)
	}
	reader.addEventListener('load', (e) => {
		log('VERB', '[post_image] upload file')
		xhr.open('POST', '/admin/edit/photo/1')
		//xhr.setRequestHeader('Content-Type', 'form/multipart')
		xhr.send(e.target.result)
	})
	f.type='file'
	f.addEventListener('change', e => {
		log('VERB', '[post_image] file choosed')
		let newImage = e.srcElement.files[0]
		reader.readAsDataURL(newImage)
	})
	log('VERB', '[post_image] choose file')
	//f.click()
	log('VERB', '[post_image] el:', e.target)
}
*/

function delete_article(elem) {
	console.log('[delete_article] ', elem)
	id = elem.target.id.split('-')[1]
	console.log('[delete_article] ' + id)
	let e_index = common_datas['{articles}'].indexOf(common_datas['{articles}'][elem])
	common_datas['{articles}'].splice(e_index, 1)
	requ('/article/' + id, {method: 'delete'})
		.then(s => elem.target.parentElement.parentElement.outerHTML = '', e => console.log('err: ', e))
		.catch(e => console.log('[delete_article] requ error: ', e))
}

function filter_list(elem) {
	if (typeof(this.way) === 'undefined')
		this.way = true
	this.way = !this.way
	let cur_filter = elem.target.id.split('-').pop()
	let datas = common_datas['{articles}']
	let list_elem = document.getElementById('list_articles')
	let ref_elem = document.getElementById('{articles}')
	datas.sort((e, a) => {
		if (this.way)
			return e[cur_filter] - a[cur_filter]
		else
			return a[cur_filter] - e[cur_filter]
	})
	while (list_elem.lastChild)
		list_elem.removeChild(list_elem.lastChild)
	list_elem.appendChild(ref_elem)
	for (i in datas) {
		let new_elem = ref_elem.cloneNode(true)
		let html_txt = new_elem.innerHTML
		new_elem.innerHTML = html_txt.replace(/{([a-z_]{0,20})}/g, name => {
			name = name.slice(1, -1)
			return datas[i][name]
		})
		list_elem.appendChild(new_elem)
		new_elem.id = new_elem.id.slice(1, -1) + '-' + datas[i].id
		new_elem.style = ''
	}
}

function attach_list_filter() {
	let filters = document.getElementsByClassName('filters')
	for (b of filters)
		b.onclick = filter_list
}

function attach_delete_buttons() {
	let delete_buttons = document.getElementsByClassName('button_delete')
	/* TODO alert */
	for (b of delete_buttons)
		b.onclick = delete_article
}

if (typeof(onready_functs) === 'object') {
	console.log('onready_functs')
	onready_functs.push(attach_list_filter)
	onready_functs.push(attach_delete_buttons)
} else {
	console.log('NO onready_functs')
	onready_functs = [
		attach_list_filter,
		attach_delete_buttons
	]
}

const handle_forms = formhandler

document.onreadystatechange = () => {
	if (document.readyState === "complete") {
		log('INFO', "[init] document.readyState complete, attaching photos input")
		file_input = document.getElementById('photos')
		file_input.onclick = post_image
	}
}

