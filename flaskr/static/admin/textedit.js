
function treat(action, txt) {
	console.log('[treat] will ' + action + ' on ' + txt)
	let new_txt = txt 
	switch (action) {
		case "title":
			new_txt = '<h3>' + txt + '</h3>'
			break;
		case "subtitle":
			new_txt = '<h4>' + txt + '</h4>'
			break;
		case "bold":
			new_txt = '<b>' + txt + '</b>'
			break;
		case "italic":
			new_txt = '<i>' + txt + '</i>'
			break;
	}
	return new_txt
}

function set_selection(ev) {
	let sel = window.getSelection(),
		rng = sel.getRangeAt(0)

	if (rng.startOffset === rng.endOffset) {
		console.log('[set_selection] no selection')
		return
	}

	console.log(sel)
	console.log(rng)

	let content_start = sel.anchorNode.parentNode.innerHTML.slice(0, rng.startOffset),
		content_end = sel.anchorNode.parentNode.innerHTML.slice(rng.endOffset),
		modified = treat(ev.target.innerHTML, sel.toString())

	sel.anchorNode.parentNode.innerHTML = content_start + modified + content_end
	console.log('[set_selection] inserted ', content_start + modified + content_end)
}

function attach_edit_buttons() {
	let buttons = document.getElementsByClassName('buttons_textedit')
	for (b of buttons)
		b.onclick = set_selection
}

onready_functs.push(attach_edit_buttons)
console.log('onready_functs: ', onready_functs)
