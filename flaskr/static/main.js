
const base_title = 'Sanei'
const base_url = document.location.protocol+'//'+document.location.host
const default_page = 'accueil'

var header = document.getElementsByTagName('header')[0],
	menu_button = document.getElementById('burgermenu'),
	form_regmail = document.getElementById('regmail')

menu_button.addEventListener('click', () => {
	let menu_visible = header.classList.toggle('menu_clicked')
	/* TODO body blur, eventListener on esc and body clic */
})

form_regmail.addEventListener('submit', e => {
    e.preventDefault()
    var post_form = new XMLHttpRequest()
    post_form.open('post', base_url + '/regmail')
    post_form.onreadystatechange = () => {
		if (post_form.readyState === 4)
			notif('Notif', post_form.response)
	}
    post_form.error = e => notif('Error', e)
    console.log('Sending: ', JSON.stringify({mail: e.target.elements[0].value}))
    post_form.send(JSON.stringify({mail: e.target.elements[0].value}))
})
