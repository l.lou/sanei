const api_url = document.location.protocol+'//'+document.location.host+'/'
var common_datas = {}

function notif(title, content, failure) {
	if (!this.notifcontainer)
		this.notifcontainer = document.getElementById('notifications')
	let div = document.createElement('div'),
		h1 = document.createElement('h1'),
		p = document.createElement('p')
	h1.innerHTML = title
	p.innerHTML = content
	div.appendChild(h1)
	div.appendChild(p)
	this.notifcontainer.appendChild(div)
	setTimeout(() => this.notifcontainer.removeChild(div), 2000)
}

function readfile(file) {
	return new Promise((resolv, reject) => {
		let raw_file = new XMLHttpRequest()
		raw_file.open('get', base_url + file)
		raw_file.onreadystatechange = () => {
			if (raw_file.readyState === 4) {
				if (raw_file.status === 200 || raw_file.status === 0) /* 0 ? */
					resolv(raw_file.responseText)
				if (raw_file.status === 404)
					reject({readfileError: '404'})
			}
		}
		raw_file.error = e => reject({readfileError: e})
		raw_file.send()
	})
}

function load_current_content() {
	/* TODO appel à /stats */
	if (document.location.pathname.startsWith('/'+default_page))
		doc = document.location.pathname.substring(default_page.length + 1 + (default_page[default_page.length - 1] === '/' ? 1 : 0))
	else
		doc = document.location.pathname
	console.log('Loading current page content : ', doc)
	replace_content(doc)
}

async function replace_content(file) {
	let params = null,
		title = null,
		pathname = null,
		elems = null
	if (file[0] === '/')
		file = file.substr(1)
	if (!file || file.length <= 1)
		file = default_page
	title = file,
		pathname = file
	if (file.indexOf('/')) {
		title = base_title
		elems = file.split('/')
		for (i in elems)
			title += ' - ' + elems[i]
		file = elems[0]
		if (typeof(elems[1]) !== 'undefined')
			params = elems[1]
	}
	document.title = title
	content = await readfile('/pages/' + file + '.html')
		.catch(error => console.log('Loading page content failed: ', error))
	let page_content = document.getElementById('page_content')
	if (params)
		content = content.replace(/{param}/g, params)
	if (pathname[pathname.length - 1] === '/')
		pathname = pathname.substring(0, pathname.length - 1)
	let url = base_url + (pathname !== default_page ? '/' + pathname : '')
	history.replaceState({}, base_title + title, url)
	if (!content) {
		page_content.innerHTML = "<h1>La page que vous cherchez n'existe pas.</h1><h3><a href=\"/\">Accueil</a></h3>"
		return
	}
	page_content.innerHTML = content
	if (typeof(handle_content) === 'function')
		await handle_content(page_content, params)
	else if (typeof(handle_content) !== 'undefined')
		console.log("[replace_content] bad type '"+handle_content+"' for handle_content param")
	register_links()
	if (typeof(handle_forms) === 'function')
		register_forms(handle_forms)
	if (typeof(onready_functs) !== 'undefined')
		for (f in onready_functs)
			onready_functs[f]()
}

function handle_link(e) {
	history.pushState({}, document.title, document.location.href)
	if (!e.currentTarget.href
		|| e.currentTarget.href.substr(0, base_url.length) !== base_url) {
		console.log('Outlink: ', e.currentTarget.href)
		console.log('/* TODO appel à /stats */')
		return
	}
	e.preventDefault()
	console.log('Activated link: ', e.currentTarget.href)
	let file = e.currentTarget.href.substr(base_url.length + (base_url[base_url.length - 1] == '/' ? 0 : 1))
	replace_content(file)
}

function register_forms() {
	let page_forms = document.getElementsByTagName('form')
	console.log('Registering forms handler')
	let len = page_forms.length
	for (let i = 0; i < len; i++) {
		console.log('[a] new: ', page_forms[i])
		if (typeof(page_forms[i].addEventListener) !== 'function')
			console.log("Can't add eventListener for : ", page_forms[i])
		else
			page_forms[i].addEventListener('submit', handle_forms, true)
	}
}

function register_links() {
	let page_links = document.getElementsByTagName('a')
	console.log('Registering links handler')
	let len = page_links.length,
		count = 0
	for (let i = 0; i < len; i++) {
		if (typeof(page_links[i].addEventListener) !== 'function')
			console.log("Can't add eventListener for : ", page_links[i])
		else {
			page_links[i].addEventListener('click', handle_link, true)
			count++;
		}
	}
	console.log('[register_links] registered ' + count + ' links')
}

function requ(url, opts={method: 'get', headers: null}, datas=null) {
	return new Promise((resolv, reject) => {
		var xhr = new XMLHttpRequest()
		xhr.open(opts.method, url, true)
		if (opts.headers) {
			for (var h of opts.headers)
				xhr.setRequestHeader(h[0], h[1])
		} else
			xhr.setRequestHeader('Accept', 'application/json')
		xhr.setRequestHeader('Content-Type', 'application/json')
		xhr.onload = function () {
			if (this.status >= 200 && this.status < 300)
				resolv(xhr.response)
			else
				reject({status: this.status, error: xhr.response})
		}
		xhr.onerror = () => {
			console.log('[requ] error: ', xhr)
			reject(xhr.response)
		}
		console.log('[requ] ' + opts.method + ' to ' + url)
		if (datas)
			console.log('       datas: ', datas)
		xhr.send( (datas ? JSON.stringify(datas) : null) )
	})
}

function replace_elem(html_element, elems) {
	if (!common_datas[html_element.id]) {
		let datas_tmp = JSON.parse(elems)
		datas = []
		if (datas_tmp && datas_tmp.length)
			for (i of datas_tmp)
				datas.push( (typeof(i) === 'string' ? JSON.parse(i) : i) )
		else if (datas_tmp)
			datas = [datas_tmp]
		common_datas[html_element.id] = datas
	} else
		datas = common_datas[html_element.id]
//	console.log('[replace_elem] datas: ', datas)
//	console.log('[replace_elem] ref el: ', html_element)
	for (d in datas) {
		let new_elem = html_element.cloneNode(true)
		let html_txt = new_elem.innerHTML
		new_elem.innerHTML = html_txt.replace(/{([a-z_]{0,20})}/g, name => {
			name = name.slice(1, -1)
			if (!datas[d])
				return null
			return datas[d][name]
		})
		new_elem.style = ''
		new_elem.classList.remove('nodisplay')
		new_elem.id = new_elem.id.slice(1, -1) + '-' + datas[d].id
		html_element.parentNode.appendChild(new_elem)
//		console.log('[replace_elem] added ', new_elem)
	}
//	console.log('[replace_elem] hidding elem: ', html_element)
	if (html_element.classList.contains('nodisplay'))
		html_element.parentNode.removeChild(html_element)
	else
		html_element.classList.add('nodisplay')
}

async function fill_datas(obj) {
	let datas = document.getElementsByClassName('{datas}')
	let l = datas.length
	let proms = []
	glob_datas_elems = datas
//	console.log('[fill_datas] elems: ', l)
	for (i = 0; i < l; i++) {
		let item = datas.item(i)
		if (/{param}/.test(item.id)) {
//			console.log('[fill_datas] found \'{param}\', clearing content')
			let html_txt = item.innerHTML
			item.innerHTML = html_txt.replace(/{([a-z_]{0,20})}/g, '')
			continue
		}
		let e = item.id.slice(1, -1)
		proms.push(requ(api_url + e)
			.then(resp => replace_elem(item, resp), err => console.log('[fill_datas] requ error: ', err))
			.catch(err => console.log('[fill_datas] replace_elem ERROR: ', err))
		)
	}
	await Promise.all(proms)
}

/*
const handle_content = fill_datas

document.onreadystatechange = () => {
	if (document.readyState === "interactive")
		load_current_content()
}

window.onpopstate = load_current_content
*/
