/*
 *	WARN	errors (mailed ?)
 *	INFO	basic info (route accessed ...)
 *	DEBUG	more info
 *	VERB	all (?) infos
 */
const log_levels = {
	'NOTHING': -1,
	'ERROR': 0,
	'WARN': 1,
	'INFO': 2,
	'DEBUG': 3,
	'VERB': 4
}
const LOGLVL = {
	NOTHING: -1,
	ERROR: 0,
	WARN: 1,
	INFO: 2,
	DEBUG: 3,
	VERB: 4
}

DEFAULT_LOG_LEVEL = 'VERB'

function log(...params) {
	p = params[0]
	let level = (typeof(p) == "number" ? p : DEFAULT_LOG_LEVEL)
	if (log_levels[DEFAULT_LOG_LEVEL || 'WARN'] >= log_levels[level]) {
		let ar_len = params.length
		console.log(level[0] + '| ', params[1])
		for (var i = 2; i < ar_len; i++)
			console.log(' | ', params[i])
		if (i > 3)
			process.stdout.write('\n')
	}
}
