import os
from flask import Flask

def create_app(test_config=None):
    app = Flask(__name__,
            instance_relative_config=True,
            instance_path=os.path.abspath(os.curdir + '/instance/'),
            static_folder=os.path.abspath(os.curdir + '/static'),
            template_folder=os.path.abspath(os.curdir + '/templates'))
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'database.sqlite'),
    )
    try:
        os.makedirs(app.instance_path)
    except OSError:
        print("[error] failed to initialize application in "+app.instance_path)
    from . import db
    db.init_db(app)
    return app
