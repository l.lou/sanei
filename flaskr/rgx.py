import re

mail = re.compile('.*\@.*\..*')
name = re.compile('^[a-z\-0-9_]{1,50}$')
id = re.compile('[0-9]{1,20}')
