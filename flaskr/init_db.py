#!/usr/bin/python

import sqlite3

db = sqlite3.connect('instance/database.sqlite')
c = db.cursor()

# TODO date auto
c.execute('''create table photos	(id integer primary key, path text,     base64 text,            	article_id integer)''')
c.execute('''create table mails 	(mail text unique,       token text,	confirmed boolean default 0)''')
c.execute('''create table steps 	(id integer primary key, lat integer,	long integer,	                comment text,	date integer)''')
c.execute('''create table categories	(id integer primary key, title text,	url_title text,	                photo text,	description text)''')
c.execute('''create table articles	(id integer primary key, cat integer,	title text,	                url_title text,	date string, state integer, photo text, content text)''')


db.commit()
db.close()
