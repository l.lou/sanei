#!/usr/bin/python
import smtplib
from . import conf as cn

#smtp_host = 'mail.gandi.net'
#smtp_port = 465
#smtp_localhostname = 'oceanevoyage.com'
## second element is port ('' = os default)
#smtp_source_address = ('51.15.208.151', '')
#smtp_user = 'oc@oceanevoyage.com'
#smtp_password = 'Semper Paratus 1997 !'
#
#from_mail = 'website@oceanevoyage.com'

def sendamail(dest, mail_content):
    smtp = smtplib.SMTP_SSL(cn.smtp.host, cn.smtp.port, cn.smtp.localhostname)
    smtp.login(cn.smtp.user, cn.smtp.password)
    smtp.sendmail(cn.smtp.from_addr, dest, mail_content)
    smtp.quit()
    return True
