import sqlite3
from . import conf
from flask import current_app, g

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def init_app(app):
    app.teardown_appcontext(close_db)

def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
                current_app.config['DATABASE'],
                detect_types=sqlite3.PARSE_DECLTYPES
                )
        g.db.row_factory = dict_factory
    return g.db

def close_db(e=None):
    db = g.pop('db', None)
    if db is not None:
        db.close()

def get(query, params=None):
    db = get_db()
    c = db.cursor()
    p = (params, ) if params and not isinstance(params, (list, tuple)) else params
    if p:
        c.execute(query, p)
    else:
        c.execute(query)
    res_list = c.fetchall()
    l = len(res_list)
    print('[db] query: ', query, ', params: ', p, ', len res: ', l)
    return res_list if l >= 1 else None

def put(query, params):
    db = get_db()
    c = db.cursor()
    c.execute(query, params)
    lastrowid = c.lastrowid
    print('[db].put lastrowid: ', lastrowid)
    db.commit()
    return lastrowid
