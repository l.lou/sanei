import flask, sqlite3, http, os, uuid
from flask import Flask, request, Response, json, render_template
from autoapp import app
from . import conf, db, mail, rgx
from .article import Article
from .mutils import *

@app.route('/regmail/', methods=['POST'])
@app.route('/regmail/<uuid:token>', methods=['GET'])
def regmail(token=None):
    if request.method == 'POST':
        datas = json.loads(request.data)
        if not datas['mail'] or not rgx.mail.match(datas['mail']):
            print('[regmail] bad mail: ', datas['mail'])
            flask.abort(Response("Adresse '%s' invalide" % datas['mail'], 400)) # todo better code 4xx
        token = str(uuid.uuid4())
        try:
            db.put('insert into mails values (?, ?, 0)', (datas['mail'], token))
        except sqlite3.IntegrityError:
            print('[regmail] already registered: ' + datas['mail'])
            flask.abort(Response("Cette adresse ('%s') est déjà enregistré" % datas['mail'], 400))
        # TODO if send fail, cancel
        print('[regmail] NOT sending mail to ', datas['mail'], ' with token: ', token)
        mail.sendamail('lpoujade@lpo.host', 'Nouvel enregistrement: ' + datas['mail'] + ', token: ', token)
        #mail.sendamail(datas['mail'], 'Cliquez sur ce lien pour etre prevenus du lancement du site: ' + token)
    elif request.method == 'GET':
        mail_addr, confirmed = db.get('select mail, confirmed from mails where token = ?', token)
        if confirmed != 1:
            db.put('update mails set confirmed = 1 where mail = ?', mail_addr)
    else:
        print('[regmail] bad request type: ', request.method)
    return flask.redirect('/')

@app.route('/article/<id_or_name>', methods=['GET'])
def one_article(id_or_name):
    # TODO verim *params expansion
    params = {'article_id': id_or_name} if rgx.id.match(id_or_name) else {'name': id_or_name}
    article = Article(**params)
    print('[get_article] article: ', article)
    if not article:
        return ('Article inexistant', 404)
    return render_template('article.html', article=article)

@app.route('/categorie/<int:id_or_name>')
def cat_getone(id_or_name):
    qtype = 'id = ?' if rgx.id.match(id_or_name) else 'url_title = ?'
    cats = db.get('select * from categories where '+qtype, id_or_name)
    if not cats or len(cats) < 1:
        return ('not found', 404)
    cats = cats.pop()
    arts = db.get('select * from articles where state = 1 and cat = ?', str(cats['id']))
    if arts:
        for article in arts:
            photos = db.get('select * from photos where article_id=:id', article['id'])
            print("[cat_getone] photos for article ", article['id'])
            article['photos'] = photos
            article['photo'] = article['photos'][0]
        cats['articles'] = arts
    if (request.headers['Accept'] == 'application/json'):
        return json.jsonify(cats)
    return render_template('categorie.html', cat=cats)

@app.route('/')
def rdr():
    return render_template('accueil.html')

@app.route('/contact', methods=['GET', 'POST'])
def contt():
    # TODO captcha & mail to oc on POST
    return render_template('contact.html')

#####     admin  routes     ######

@app.route('/admin')
def admin():
    datas = {}
    datas['articles'] = db.get('select * from articles')
    datas['categories'] = db.get('select * from categories')
    datas['photos'] = None
    datas['steps'] = None
    return render_template('admin/admin.html', datas=datas)

@app.route('/admin/edit/<item_type>/', defaults={'item_id': None}, methods=['GET'])
@app.route('/admin/edit/<item_type>/<item_id>', methods=['GET'])
def admin_get_edit(item_type, item_id=None):
    r = request
    if item_type == "article":
        # item_id = r.data['id'] if (r.data and r.data['id']) else None

        art = Article(article_id=item_id) if item_id else None
        cats = db.get('select * from categories')
        ret = render_template('admin/edit.html', art=art, categories=cats)
    elif item_type == "categorie":
        if id_or_name == -1:
            return render_template('admin/edit_cat.html', cat={'id': -1})
        qtype = 'id = ?' if rgx.id.match(id_or_name) else 'url_title = ?'
        cat = db.get('select * from categories where '+qtype, id_or_name)
        if cat and len(cat) == 1:
            cat = cat.pop()
        ret = render_template('admin/edit_cat.html', cat=cat)
    return ret

@app.route('/admin/edit/<item_type>', methods=['POST'])
def admin_post_edit(item_type):
    print('[admin_post_edit] item_type / datas', item_type)
    datas = request.form if not request.get_json() else request.get_json()
    print(datas)

    if not datas or not 'id' in datas.keys():
        print("[admin_post_edit] bad datas")
        return ("bad datas", 404)

    if item_type == 'article':
        item = Article(article_id=datas['id'], datas=datas)
    else:
        # TODO category
        print("[admin_post_edit] update category ", datas.id)
        db.put('update categories set title=:title, url_title=:url_title, description=:desc where id=:id', datas)
    print('[edit] redirect to /admin/edit/'+item_type+'/'+str(item.id))
    return flask.redirect('/admin/edit/'+item_type+'/'+str(item.id))

@app.route('/admin/addphoto/<int:artid>', methods=['POST'])
def admin_post_photo(artid):
    datas = json.loads(request.data)
    print("[admin_post_photo] len:", len(datas['base64']), ", article_id: ", artid)
    if (int(artid) != -1):
        item = Article(article_id=artid)
        item.add_photo(datas)
        return 'ok'
    else:
        return ('please save the article before', 404)

#@app.route('/admin/upload', methods=['POST'])
#def upload_file():
#    f = request.files['th_file?']
#    f.save(app.instance_path + '/uploads/' + __FILE_NAME__)

@app.route('/admin/delete/article/<int:artid>', methods=['GET'])
def del_article(artid):
    print('[del_article] id:', artid)
    query = 'delete from articles where id=:artid'
    db.put(query, {'artid': artid})
    return flask.redirect('/admin')
