import sqlite3
from . import rgx, db
from .mutils import *

class Article():
    id = -1
    title = 'Titre par défaut'
    url_title = 'titre-defaut'
    date = '2019-01-01'
    photo = None
    photos = []
    content = '<h1>Contenu par défaut</h1>'
    state = 0
    cat = 0

    def __init__(self, **kw):
        """"
            Crée un objet Article en fonctions des arguments:
            { id: x }               charge l'article avec l'id x
            { datas: {...}}         crée un article avec les données de datas
            { id: x, datas: {...}}  modifie l'article x avec datas
        """
        if (not 'article_id' in kw.keys()) or (not kw['article_id']):
            print('[Article] new article', kw)
            if kw['datas']:
                for d in kw['datas']:
                    print('[Article] new article data: ', d, ' = ', kw['datas'][d])
                    self[d] = kw['datas'][d]
            self.save()
            return
        print('[Article] load article', kw['article_id'])
        datas = db.get('select * from articles where id = ?', kw['article_id'])
        if not datas or len(datas) != 1:
            print('[Article] no article: ', datas)
            return None
        article_datas = datas.pop()
        print('[Article] datas:', datas)
        self.update(article_datas)
        return

    @classmethod
    def find(**kw):
        print('[Article] load article', kw['article_id'])
        datas = db.get('select * from articles where id = ?', kw['article_id'])
        if not datas or len(datas) != 1:
            print('[Article] no article: ', datas)
            return None
        article_datas = datas.pop()
        print('[Article] datas:', datas)
        self.update(article_datas)


    def __setitem__(self, k, v):
        setattr(self, k, v)

    def __getitem__(self, k):
        return getattr(self, k)

    def update(self, datas):
        for i in datas:
            setattr(self, i, datas[i])
        self.save()

    def namedprops(self):
        s = self
        return {'id': s.id, 'title': s.title, 'url_title': s.url_title, 'cat': s.cat, 'state': s.state, 'content': s.content, 'date': s.date}

    def __str__(self):
        s = self
        return '[Article:'+str(s.id)+'] '+str(s.cat)+' '+s.title+' '+s.url_title+' '+str(s.state)

    def add_photo(self, photo):
        if not self.photos:
            self.photos = []
        self.photos.append(photo)
        query = 'insert into photos (base64, article_id) values (:base64, :article_id)'
        print("[add_photo] new photo for ", self.id)
        lastrowid = db.put(query, (photo['base64'], self.id))
        return lastrowid
        
    def save(self):
        if self.id:
            print('[articles] updating ', self.namedprops())
            query = 'update articles set cat=:cat, title=:title, url_title=:url_title, date=:date, state=:state, content=:content where id=:id'
        else:
            print('[articles] adding ', self)
            query = 'insert into articles ('
            init_len = len(query)
            to_append = ''
            for prop in 'cat', 'title', 'url_title', 'date', 'state', 'content':
                if (self[prop]):
                    if (len(query) > init_len):
                        query += ','
                    query += prop
                    if (len(to_append) > 1):
                        to_append += ','
                    to_append += ':' + prop
            query += ') values (' + to_append + ')'
        lastrowid = db.put(query, self.namedprops())
        if not self.id:
            self.id = lastrowid
            print('[articles] added  ', self)
