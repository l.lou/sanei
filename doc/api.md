API
-- 
> need `Accept-Content = 'application/json'`  
> retourne du JSON  
> routes public en HTTP GET  
> routes privées: POST & nécessitent un token  
> token obtenu au premier accès à la page admin  


> la requête pourrait préciser un header LANGUAGE  
> header obligatoire dans ce cas  


endpoints
--

### GET
* /
* `/categorie/<x>`: détails de la catégorie x
* `/categories`: liste des categories
* `/article/<x>`: article x
* `/articles/<cat>`: liste des articles de la categorie cat
	* tous les articles si cat=None

### POST
* `/article`: modifie/enregistre un article
* `/categorie`: modifie/enregistre une categorie
* `/photo/<linkid>`:
	* upload une photo
	* si linkid: ajoute la photo en db pour l'article/la catégorie linkid
	* linkid: article (bannière), catégorie (bannière), null 'photo du moment'


## POST/GET
* `/stats`: envoyer/recevoir les statistiques de clics
	* enregistrement de liens sortants
	* des arrivées sur le site (et du referer si dispo)
	* du type de navigateur (?)
	* des clics internes (en back?)
* `/regmail`: enregistrer/valider une adresse mail pour newsletter/etc

### _get_ articles [categorie]

    [ 
		{
    		id: int,
    		url_title: 'article-name',
    		title: 'Article name',
    		date: 2018-01-01
		},
		{
			id: int,
			...
		},
		...
	]

### _get_ article [id|url\_title]
> met à jour _db.stats_ :


| type		| outurl		| localurl		| count |
|-----------|:-------------:|:-------------:|:-----:|
| viewscount| ''			|"http...title"	|	+1	|

    {
		id: int,
		url_title: 'article-name',
		title: 'Article name',
		date: 2018-01-01,
		content: 'raw_html'
	}


### _post_ article/categorie
> nécessite un token valide  
> données: objet article
> créer ou met à jour ( en fonction d'un id ou non )
> si création: tout les champs obligatoires sauf photo

db
- 

### tables
* articles
* categories
* photos _toutes les photos (articles, categories, moments)_
* stats _chaque clic sur un lien avec nombre de clic_
* mails

#### **mails**
* id
* mail

#### **articles**
* id
* cat _categorie id_
* title _string_
* url\_title _string_
* date
* photo _id photo_
* content _html/markdown_
* state _int_

#### **categorie**
* id
* title _string_
* url\_title _string_
* desc _string/text_
* photo _id photo_

#### **photos**
* id
* linkid _id article ou id categorie_
	* _si null, 'photos du moment'_
* path
* date _date added_

#### **stats**
* id _int_ ?
* type _int_ [1: outlink, 2: referer, 3: viewscount]
* outurl _text_
* localurl _text_
* count _int_

