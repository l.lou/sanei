## content
* catégorie d'articles
	* titre
	* photo
* article
	* titre
	* photo
	* date
	* contenu
		* texte / media
	* page
		* header: bannière + titre
		* texte / media
* blog
	* articles
	* flux rss / envoi mails
	* page d'index
		* articles/billets classés par dates
		* photo / titre -> page article


## plan du site
> `<header>` présent partout

* /:			accueil
* /qui-suis-je:	apropos
* /sponsors:	sponsors
* /contact:		contact
* différentes catégories d'article, avec pour chaque catégorie une page listant les articles
	* /voyages , /conseils-de-voyage , /maladie-de-lyme , /blog | /des-steppes-mongoles-au-piemont-cevenol
	* /voyages , /conseils , /lyme , /blog | /des-steppes-mongoles-au-piemont-cevenol
* blog, avec quelques articles avant les billets de blog

## détails des pages

### layout général
* header: accueil - me contacter - menu
* footer: abonnement mail - liens ? - crédits

### accueil
* section présentation
	* liens social (youtube, facebook, instagram, partage)
	* image bannière + logo centré
	* navbar, liens principaux
* section promo
	* encart "Suivre son propre chemin" - texte - logo
	* séparation barre gris/noir
	* actus: dernier billet de blog - photo du moment
* section «Steppes au Piémont»
	* 3 photos background
	* titre, texte présentation
	* lien vers blog
* footer: abonnement mail - contact

### qui suis je
* texte photos (contenu de l'article)

### voyages
* liste pays
* pays: une page avec un récit
* classés par date

### maladie de lyme
* différentes sections sur la même page
* une section = texte / 1 photo

### sponsors
* liste de sponsor
