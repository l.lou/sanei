## pages dispo liées à l'édition
* liste des articles
	* nouveau
	* modifier
	* supprimer
	* changer l'état
* ajout position / étape
	* liste des étapes précédentes
* photo du moment
	* nouvelle photo (upload)
	* liste des anciennes photos (bouton suppression)
* liste des catégories
	* modifier
	* supprimer

### urls
* `/edit/<article_name>`
* `/new`


## éditeur d'article

> form  
> modification ou création d'article  
> ajout de fichier (images)

* title: `input` titre
* url\_title: `input` titre 'simplifié' pour url
* cat: `select` catégorie (voyages, lyme, blog)
	* choix spécial: apropos, sponsors
	* si choix spécial checké, select vérouillé
* content: `textarea` html + plugin éditeur

## éditeur de catégorie

> form titre, photo
